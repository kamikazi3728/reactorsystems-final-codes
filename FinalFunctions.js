

//Exam2functions.js

const colors = require('colors');

//function to start new part to the question
function startNewPart(n){
	console.log("============================================================================================");
	console.log("========================================== PART "+n+" ==========================================");
	console.log();
}

//function to display blank line
function blank(){console.log();}

//Function to display code information
function displayInfo(title,author,dayString,part){
	console.log(title);console.log("Code written by: "+author);
	console.log("Code completed on: "+dayString);
	blank();
	startNewPart(part);
}

//A line of stars :)
let starLine = "******************************"; 

//function for solution header
function solnHeader(prbn,prt){
	colorOut([starLine," Solution for Problem "+prbn+", Part "+prt+" ",starLine],[cyan,white,cyan]);
	blank();
}

//function to write a string in a color
function colorme(value,color){
	return eval("(value.toString())."+color);//USES EVAL, FIND A WORKAROUND!!!!---------------------
}

//function to write a string with varying colors throughout specified values
function colorOut(strs,clrs){
	let sout = "";
	for(var i=0;i<strs.length;i++){
		sout=sout+colorme(strs[i],clrs[i]);
	}
	console.log(sout);
}

//function that returns a string of 37 spaces
function spaces(){
	let nsp = 37;
	let spo = "";
	for(var i=0;i<nsp;i++){
		spo=spo+" ";
	}
	return spo;
}

//Defining color constants as strings, because quotation marks
const green = "green";
const cyan = "cyan";
const yellow = "yellow";
const magenta = "magenta";
const white = "white", red="red";
const w=white,g=green,c=cyan,y=yellow,m=magenta,r=red;

//create function for simple outputs with alternating white and yellow info (a=arguments to print) (internally, b=colors array)
function show(...a){
	let b=[];
	for(var i=0;i<a.length;i++){
		((((i+1)%2)>0)?b.push(w):b.push(y));
	}
	colorOut(a,b);
}

//create function to set and show (ONE VARIABLE ONLY)
function set(a,b,c){
	show(a," "+b+" ",c);
	return b;
}

//create function for averaging two numbers
function avg(a,b){
	return ((a+b)/2);
}

//Set up interpolation function
function interpolate(pt1,pt2,pt3x){
	let slope=(pt2[1]-pt1[1])/(pt2[0]-pt1[0]);
	return (pt1[1]+(slope*(pt3x-(pt1[0]))));
}

//constructor for custom errors
function err(code,data){
	this.code=code;
	this.key=()=>{return this.code;};
	this.data=data;
	this.info=()=>{return this.data;};
}

//function to output all red errors
function errmsg(...msgArr){
	let rArr = [];
	for(var i=0;i<msgArr.length;i++){
		rArr.push(r);
	}
	colorOut(msgArr,rArr);
}

//Set up exports
exports.snp = startNewPart;
exports.blk = blank;
exports.dI = displayInfo;
exports.sL = "******************************";
exports.sH = solnHeader;
exports.cme = colorme;
exports.c = {g:"green",c:"cyan",y:"yellow",m:"magenta",w:"white",r:"red"};//Defining color constants as strings, because quotation marks
exports.cO = colorOut;
exports.sp = spaces;
exports.show = show;
exports.set = set;
exports.avg = avg;
exports.interpolate = interpolate;
exports.err = err;
exports.errmsg = errmsg;
import json
import sys

#PYTHON STEAMTABLES
from iapws import IAPWS95

if sys.argv[3] == "PT":
	water=IAPWS95(P=float(sys.argv[1]), T=float(sys.argv[2]))
	
if sys.argv[3] == "PH":
	water=IAPWS95(P=float(sys.argv[1]), h=float(sys.argv[2]))

print(water.T)#kelvin
print(water.P)#MPa
print(water.h)#kJ/kg
print(water.cp)#kJ/kg K
print(water.mu)#Pa s
print(water.rho)#kg/m3
print(water.k)#W/m K
print(water.u)#kj/kg
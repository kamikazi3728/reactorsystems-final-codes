const {PythonShell} = require('python-shell');

function pyInt(pyPath,scriptPath){
	this.options = {
		mode: 'text',
		pythonPath: pyPath,
		pythonOptions: ['-u'],
		scriptPath: scriptPath,
		args: []
	};
	this.run = function(arguments){
		return new Promise((resolve,reject)=>{
			this.options.args = [];
			for(var i=1;i<arguments.length;i++){
				this.options.args.push(arguments[i]);
			}
			PythonShell.run(arguments[0],this.options, function(err,outArray){
				if(err){reject(err)};
				resolve(outArray);
			});
		});
		
	};
}

exports.setup = function(pyPath,scPath){
	return new pyInt(pyPath,scPath);
};
module.exports = {
	'met':{
		'h':'kJ/kg',
		's':'kJ/K kg',
		'p':'Pa',
		't':'K',
		'rho':'kg/m^3',
		'v':'m^3/kg',
		'u':'kJ/kg',
		'cp':'kJ/kg degC',// MAY BE kJ/kg K ----------------UNSURE, 0 documentation on what the default is-----------------
		'w':'m/s',
		'mu':'kg/m s',
		'k':'W/m K'
	},
	'emp':{
		'h':'Btu/lb',
		's':'Btu/lb degF',
		'p':'psi',
		't':'F',
		'rho':'lb/ft^3',
		'v':'ft^3/lb',
		'u':'Btu/lb',
		'cp':'Btu/lb degF',
		'w':'ft/hr',
		'mu':'lb/ft hr',
		'k':'Btu/hr ft degF'
	}
}
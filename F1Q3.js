/*     NE405/505 Final Exam Question 3 Code
		By: Reece Appling
*/
//conversion factors and constants
const MWToBtuPerHr = 3412141.6331279;
const BtuPerHrToMW = 1/MWToBtuPerHr;
const pi = Math.PI;
const gc = 32.2*(3600**2); //Gravitational constant

//Set all problem parameters
let nominalQ = 2786*MWToBtuPerHr;console.log("Nominal Q (Btu/hr): "+nominalQ);
let P = 1045;
let Tfeed = [440,400];
let nominalm = 77*(10**6);
let S = 0.64/12;
let Do = 0.483/12;
let waterRodDo = 0.591/12;
const H = 150/12;
let Fz = 1.4;
let canDim = (5.278**2)/144;
let lattice = 8*8;
let NA = 560;
let waterRodsPerAssemb = 2;
let gamma = 0.91;
let voidCoeff = -91;// pcm/%void
let dopplerCoeff = -9.8;// pcm/%Power

//Define later-used variables
var Q;var xoutNew;var pcmNominal = 0;

//Calculated variables
const hfeed = [419.445,375.963];
const hg = 1190.85;
const hf = 549.498;
const hfg = hg-hf;console.log("hfg: "+hfg);
const rhog = 2.35527;
const rhof = 45.9519;
const theta = 0.0171775*0.224809*(3600**2);console.log("Theta: "+theta+"lb/hr^2");//lb/hr^2

//rewriting sin and cos functions
function sin(x){
	return Math.sin(x);
}
function cos(x){
	return Math.cos(x);
}

//Directive function for iterative functions
async function iterate(eqOneIn,eqTwoIn,bounds,method,acc){
	//set temporary function variables
	let eqOne = eqOneIn;
	let eqTwo = eqTwoIn;
	if(typeof eqOne!="function"){eqOne=function(x){return eqOneIn;};}//creates an arbitrary function if eq1 is a number instead of a function
	if(typeof eqTwo!="function"){eqTwo=function(x){return eqTwoIn;};}//creates an arbitrary function if eq2 is a number instead of a function
	switch(method){
		case "falsePosition":
			return falsePositionMethod(eqOne,eqTwo,bounds,acc);
			break;
		case "bisection":
			return bisectionMethod(eqOne,eqTwo,bounds,acc);
			break;
		default:
			console.log("Method "+method+" not recognized for iteration, falling back to bisection method!");
			return bisectionMethod(eqOne,eqTwo,bounds,acc);
	}
}

//False position method of finding roots in transcendental equations
function falsePositionMethod(funcOne,funcTwo,boundsOrig,acc){
	return new Promise((accept,reject)=>{
		let compoundFunction = function(x){return (funcOne(x)-funcTwo(x));};
		let newBounds = boundsOrig,lastxn=0;
		while(true){
			let toGo = newBounds;
			let xn = toGo[0]-(((toGo[1]-toGo[0])*compoundFunction(toGo[0]))/(compoundFunction(toGo[1])-compoundFunction(toGo[0])));
			if(Math.abs(xn-lastxn)<acc){accept(xn);break;}
			if(compoundFunction(xn)==0){accept(xn);break;}
			if(Math.sign(compoundFunction(xn))==NaN){reject('ERROR! ITERATION YIELDED NaN!');}
			if(compoundFunction(xn)*compoundFunction(toGo[0])>0){newBounds[0]=xn;}else{newBounds[1]=xn;}
			if(Math.abs(newBounds[0]-newBounds[1])<acc){accept(xn);break;}
			lastxn = xn;
		}
		reject(falsePosition);
	});
}

//Bisection method of finding roots in transcendental equations
function bisectionMethod(funcOne,funcTwo,bounds,acc){
	return new Promise((accept,reject)=>{
		let newBounds=bounds;
		let lastxn=bounds[0];
		let compoundFunction = function(x){return (funcOne(x)-funcTwo(x));};
		while(true){
			let midPt = (newBounds[0]+newBounds[1])/2;
			if(compoundFunction(midPt)==0||(Math.abs(midPt-newBounds[0])<acc&&Math.abs(midPt-newBounds[1])<acc)){
				accept(midPt);
				break;
			}
			else{
				lastxn=midPt;
				if(Math.sign(compoundFunction(newBounds[0]))==Math.sign(compoundFunction(midPt))){newBounds[0]=midPt;}
				else{if(Math.sign(compoundFunction(newBounds[1]))==Math.sign(compoundFunction(midPt))){newBounds[1]=midPt;}
					else{
						reject('ERROR! BISECTION ITERATION YIELDED NaN!');
						break;
					}
				}
			}
		}
	});
}

//function for the numerical integral of an arbitrary function over an arbitrary range (WORKING)
function integral(fxn,bounds,parts){
	let dx = (bounds[1]-bounds[0])/parts;
	let summation=0, currentVal=0, currentX = 0;
	for(var i=1;i<=parts;i++){
		currentX=bounds[0]+(i*dx);
		currentVal=fxn(currentX)*dx;
		summation=summation+currentVal;
	}
	return summation;
}

//function for equilibrium quality
function xe(h){return ((h-hf)/hfg);}

//functions and constants for void calculations
const Vgj = 2.9*(((theta*gc*(rhof-rhog))/(rhof**2))**(1/4));console.log("Vgj: "+Vgj);
const b = ((rhog/rhof)**0.1);console.log("b: "+b);
function beta(x){return (x/(x+((1-x)*(rhog/rhof))));}
function Co(x){let betax = beta(x);return (betax*(1+(((1/betax)-1)**b)));}
function alpha(x,G){
	if(x<=0){return 0;}
	else{
		return (((Co(x)*(1+(((1-x)/x)*(rhog/rhof))))+(rhog*Vgj/(G*x)))**(0-1));
	}
}

//Calculate number of rods
let nrods = (lattice-2)*NA;
console.log("Number of fuel rods: "+nrods);
let nWrods = NA*waterRodsPerAssemb;
console.log("# of water rods: "+nWrods);
let Arod = pi*(Do**2)/4;
console.log("Cross-sectional Area of a fuel rod "+Arod+" ft^2");
let AWrod = pi*(waterRodDo**2)/4;
console.log("Cross-sectional Area of a water rod "+AWrod+" ft^2");
//calculate total rod SA
let hxArea = nrods*pi*Do*H;
console.log("Total heatup surface area: "+hxArea+" ft^2");
//Calculate AxTotal
let AxTotal = (canDim*NA)-(Arod*nrods)-(AWrod*nWrods);
console.log("Total core flow Area "+AxTotal+" ft^2");
//calculate nominal G
let nominalG = nominalm/AxTotal;
console.log("Nominal mass flux (lbm/hr) "+nominalG);
//2 blank lines
console.log();
console.log();



//Calculate lambda and He
let xmax = 2.029; //********Refer to NE405 Hw2 Problem 3 solution for how this was found
console.log("xmax = "+xmax);
function HeCalc(lambdaTemp){return H+(2*lambdaTemp);}
function lambdaCalc(lamb){
	let HeTemp = HeCalc(lamb);
	let tempone = pi*lamb/HeTemp;
	let temptwo = pi*(H+lamb)/HeTemp;
	let toOut = ((lamb*cos(tempone))-((H+lamb)*cos(temptwo))-((HeTemp/pi)*sin(tempone))+((HeTemp/pi)*sin(temptwo)));
	let toOutt = (((xmax*Math.sin(xmax))/(toOut*(1/H)))-Fz);
	return toOutt;
}

//Iterate to find lambda
let lambdaAccuracy = 1/10000;
console.log("Iterating to find lambda...");
iterate(lambdaCalc,0,[0.0001,5],"bisection",lambdaAccuracy).then((foundLambda)=>{
	console.log("H = "+H+" ft");
	let lambda = foundLambda;
	console.log("Lambda Found: "+lambda+" ft");
	let He = HeCalc(lambda);
	console.log("He Found: "+He+" ft");
	console.log("Nominal Q: "+nominalQ);
	//Calculate mfeed
	let mFeed = nominalQ/(hg-hfeed[0]);
	console.log("mFeed: "+mFeed+" lbm/hr");
	let mfeed = mFeed;
	//Calculate xExit
	let xExit = mFeed/nominalm;
	console.log("Nominal core exit quality: "+xExit);
	//Calculate hExit
	let hExit = (xExit*hfg)+hf;
	console.log("Nominal core exit enthalpy: "+hExit);
	//Calculate hin
	let hin = hExit-(nominalQ/nominalm);
	console.log("Nominal core inlet enthalpy: "+hin);
	//Calculate Average nominal heat flux
	let nominalqppavg = nominalQ*gamma/hxArea;
	console.log("Average nominal heat flux: "+nominalqppavg);
	//Calculate qo''
	let temponea = pi*lambda/He;
	let temptwoa = pi*(H+lambda)/He;
	let nominalqo = ((lambda*cos(temponea))-((H+lambda)*cos(temptwoa))-((He/pi)*sin(temponea))+((He/pi)*sin(temptwoa)));
	nominalqo = (nominalqppavg*H/nominalqo);
	console.log("Nominal q\'\'o: "+nominalqo);
	//Define enthalpy distribution
	let nominalh = (z)=>{
		let tone = pi*(H+lambda-z)/He;
		let ttwo = pi*(H+lambda)/He;
		let toh = ((tone*cos(tone))-(ttwo*cos(ttwo))-sin(tone)+sin(ttwo));
		toh = toh*nrods*nominalqo*He*Do/(nominalm*gamma);
		return (hin+toh);
	};
	//Numerically integrate to find void
	let nomVoid = (z)=>{
		let nG=nominalG;
		let xa = (nominalh(z)-hf)/hfg;
		return alpha(xa,nG);
	};
	let voidIntegralParts=H*12*100;//dH of void iteration (100th of an inch)
	let nominalVoid = (100*integral(nomVoid,[0,H],voidIntegralParts)/H);//Nominal void in PERCENT
	console.log("Nominal void average: "+nominalVoid+"%");
	//Save void and Power reactivity value
	pcmNominal = (100*dopplerCoeff)+(voidCoeff*nominalVoid);
	console.log("Nominal pcm change from Doppler and Void: "+pcmNominal);
	//Calculate new Q
	console.log("dh across transient feed loop: "+(hg-hfeed[1]));
	let Q = mFeed*(hg-hfeed[1]);
	console.log("New Q: "+Q+" (Btu/hr), "+Q*BtuPerHrToMW+" MW, "+(Q/nominalQ)*100+"%");
	//Finding doppler for transient
	let doppler = (Q*100*dopplerCoeff/nominalQ);
	console.log("Doppler from heatup (pcm) "+doppler);
	let reqdpcm = pcmNominal-doppler;
	console.log("Required void pcm: "+reqdpcm);
	console.log();console.log();console.log();
	console.log("Iterating to find new Values...");
	var mCore=0,G=0,voidWorth=0,qppavg=0,qo=0,newhin=0,xi=0,transientVoid=0;
	let iParts = 1000000;
	for(var i=173930;i<=iParts;i++){
		//calculate core outlet quality to check
		xi = (1/iParts)*i;
		//Use Xe to find mCore
		mCore = mFeed/xi;
		//Calculate new G
		G = mCore/AxTotal;
		//Use mCore, mFeed, hf, and hfeed to calc hin
		newhin = ((hf*(mCore-mFeed))+(mFeed*hfeed[1]))/mCore;
		hin = newhin;
		//Calculate qppavg
		qppavg = Q*gamma/hxArea;
		//calculate qo
		qo = ((lambda*cos(temponea))-((H+lambda)*cos(temptwoa))-((He/pi)*sin(temponea))+((He/pi)*sin(temptwoa)));
		qo = (qppavg*H/qo);
		//Define enthalpy distribution
		let newh = (z)=>{
			let tone = pi*(H+lambda-z)/He;
			let ttwo = pi*(H+lambda)/He;
			let toh = ((tone*cos(tone))-(ttwo*cos(ttwo))-sin(tone)+sin(ttwo));
			let nhi = newhin;
			toh = toh*nrods*qo*He*Do/(mCore*gamma);
			return (nhi+toh);
		};
		//Numerically integrate to find void
		let newVoid = (z)=>{
			let nG=G;
			let xa = (newh(z)-hf)/hfg;
			return alpha(xa,nG);
		};
		transientVoid = (100*integral(newVoid,[0,H],voidIntegralParts)/H);//Nominal void in PERCENT
		voidWorth = transientVoid*voidCoeff;//worth of void
		//break loop if void worth is less than required
		if((voidWorth**2)>(reqdpcm**2)){
			break;
		}
	}
	//Display solutions
	console.log("Results...");
	console.log("New Q: "+Q+" (Btu/hr), "+Q*BtuPerHrToMW+" MW, "+(Q/nominalQ)*100+"%");
	console.log("Outlet quality: "+xi);
	console.log("Core mass flow rate (lbm/hr) "+mCore);
	console.log("Mass flux: "+G);
	console.log("New inlet enthalpy: "+hin);
	console.log("average heat flux: "+qppavg);
	console.log("qo: "+qo);
	console.log("Average void: "+transientVoid);
	console.log("Void worth "+voidWorth);
	console.log("Power worth: "+doppler);
	console.log("Combined worth: "+(voidWorth+doppler));
	console.log("which must be more negative than "+pcmNominal);
	console.log("resulting change in pcm :"+(voidWorth-reqdpcm));	
},(lambdaError)=>{console.log(lambdaError);});
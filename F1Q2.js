//import filesystem library
const fs = require('fs');

let filenames = ["BOP","Pressurizer","RxCore","SG"];
let tree = [0,0,0,0];
tree[0] = ["BOC","EOC"];
tree[1] = ["automatic","manual"];
tree[2] = ["heatersOff","heatersOn"];
tree[3] = ["tbvOff","tbvOn"];
let dirStart = "problem2";
function DirAdd(parents,child){
	return parents+'/'+child;
}

//Read lines from file
function readLines(input, func) {
  var remaining = '';
  input.on('data', function(data) {
    remaining += data;
    var index = remaining.indexOf('\n');
    var last  = 0;
    while (index > -1) {
      var line = remaining.substring(last, index);
      last = index + 1;
      func(line);
      index = remaining.indexOf('\n', last);
    }
    remaining = remaining.substring(last);
  });

  input.on('end', function() {
    if (remaining.length > 0) {
      func(remaining);
    }
  });
}



//call reading and writing functions
function readwritefile(ii,jj,kk,ll,mm){
	let fileWithoutType = dirStart+"/"+tree[0][ii]+"/"+tree[1][jj]+"/"+tree[2][kk]+"/"+tree[3][ll]+"/"+filenames[mm]
	var arrin = fs.readFileSync(fileWithoutType+'.dat').toString().split("\n");
	arrin.shift();
	
	let outfile = fs.createWriteStream(fileWithoutType+'.csv');
	outfile.on('error', function(err) {
		console.log("Error encountered");
		console.log(err);
	});
	for(str in arrin){
		if(str<arrin.length-2){
			outfile.write(arrin[str].match(/[-]{0,}[0-9]{0,}[.][0-9]{1,}[E][\+-][0-9]{2}/g).join(',')+'\n');
		}
	}
	outfile.end();
}

//read dat files to csv
for(var i=0;i<2;i++){
	for(var j=0;j<2;j++){
		for(var k=0;k<2;k++){
			for(var l=0;l<2;l++){
				for(var m=0;m<4;m++){
					readwritefile(i,j,k,l,m);
				}
			}
		}
	}
}

function getColumn(arr,m){
	let toOut = [];
	for(var i=0;i<arr.length;i++){
		toOut.push(arr[i][m]);
	}
	return toOut;
}

//Create file for x
function csvOut(mm,filename,col){
	let arrayOut=[];
	//read in csv files to output graphs
	let timeArr = ["time (s)"];
	let fileData = [];
	for(var i=0;i<2;i++){fileData.push([]);
		for(var j=0;j<2;j++){fileData[i].push([]);
			for(var k=0;k<2;k++){fileData[i][j].push([]);
				for(var l=0;l<2;l++){fileData[i][j][k].push([]);
						let currentLength = arrayOut.length;
						let toAppend = 0;
						toAppend = [];	
						fileData[i][j][k][l].push(tree[0][i]+" "+tree[1][j]+" "+tree[2][k]+" "+tree[3][l]);
						let fileWithoutType = dirStart+"/"+tree[0][i]+"/"+tree[1][j]+"/"+tree[2][k]+"/"+tree[3][l]+"/"+filenames[mm];
						var arrin = fs.readFileSync(fileWithoutType+'.dat').toString().split("\n");
						arrin.shift();
						for(str in arrin){
							if(str<arrin.length-2){
								let selection = arrin[str].match(/[-]{0,}[0-9]{0,}[.][0-9]{1,}[E][\+-][0-9]{2}/g);
								if(i==0&&j==0&&k==0&&l==0){
									timeArr.push(selection[0]);
								}
								fileData[i][j][k][l].push(selection[col]);
							}
						}
				}
			}
		}
	}
	//Output compound csv's
	let finalOut = [];
	for(var z=0;z<timeArr.length;z++){
		let rowArr = [timeArr[z]];
		for(var i=0;i<2;i++){
			for(var j=0;j<2;j++){
				for(var k=0;k<2;k++){
					for(var l=0;l<2;l++){
						rowArr.push(fileData[i][j][k][l][z]);
					}
				}
			}
		}
		finalOut.push(rowArr.join(","));
	}
	//output compound csvs
	let outfil = fs.createWriteStream(dirStart+"/"+filename+".csv");
	outfil.on('error', function(err) {
		console.log("Error encountered");
		console.log(err);
	});
	finalOut.forEach(function(v){outfil.write(v+'\n');});
	outfil.end();
}

//Generate each output csv
csvOut(2,"ReactivityRods",29);
csvOut(2,"ReactivityFuel",26);
csvOut(2,"ReactivityMod",27);
csvOut(2,"Tave",10);
csvOut(2,"RxPower",1);
csvOut(2,"Coremdot",4);
csvOut(2,"Centerline",8);
csvOut(2,"MDNBR",22);
csvOut(2,"Reactivity",25);
csvOut(1,"PrimaryP",1);
csvOut(1,"PrzP",2);
csvOut(1,"PrimaryP",1);
csvOut(0,"Wturb",23);
csvOut(0,"Wload",24);
csvOut(0,"TCVPos",13);
csvOut(0,"TBVPos",25);